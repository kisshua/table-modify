## # 1.5.4
- x 新增忽略表更新功能
- x 去掉mybatis plus的强制依赖
- x 更改为Spring Data Jdbc进行数据操作
- x 增加PostgreSql对应自动建表
- x 支持字节数组的默认映射
## 1.5.5
- 支持java原生持久化注解
- 增加PostgreSql对应的自动更新表结构
- 支持对单独表配置先删除再创建表
- 修改更新MySQL字段注释的时候自动加一个逗号

