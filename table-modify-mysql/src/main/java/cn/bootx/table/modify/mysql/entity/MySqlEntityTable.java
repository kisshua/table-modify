package cn.bootx.table.modify.mysql.entity;

import cn.bootx.table.modify.constants.TableCharset;
import cn.bootx.table.modify.mysql.constants.MySqlEngineEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 数据表信息
 * @author xxm
 * @since 2023/4/14
 */
@Getter
@Setter
@Accessors(chain = true)
public class MySqlEntityTable {

    /** 表名称 */
    private String name;

    /** 表注释 */
    private String comment;

    /** 字符集 */
    private TableCharset charset;

    /** 存储引擎 */
    private MySqlEngineEnum engine;

    /** 主键列表 */
    private List<String> keys;

}
