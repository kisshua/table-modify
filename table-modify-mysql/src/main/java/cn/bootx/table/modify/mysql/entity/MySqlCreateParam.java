package cn.bootx.table.modify.mysql.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 建表参数
 * @author xxm
 * @since 2023/4/18
 */
@Getter
@Setter
@Accessors(chain = true)
public class MySqlCreateParam {
    /** 表名称 */
    private String name;

    /** 表注释 */
    private String comment;

    /** 字符集 */
    private String charset;

    /** 存储引擎 */
    private String engine;

    /** 行列表 */
    private List<String> columns = new ArrayList<>();

    /** 主键列表 */
    private String keys;

    /** 索引列表 */
    private List<String> indexes = new ArrayList<>();
}
