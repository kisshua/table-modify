package cn.bootx.table.modify.mysql.constants;

import cn.bootx.table.modify.mysql.entity.MySqlTypeAndLength;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Mysql和Java类型映射关系
 *
 * @author sunchenbin
 * @since 2020/5/27
 */
public class MySql4JavaType {

    private static final Map<String, MySqlFieldTypeEnum> TYPE_MAP = new HashMap<>();
    static {
        TYPE_MAP.put("java.lang.String", MySqlFieldTypeEnum.VARCHAR);
        TYPE_MAP.put("java.lang.Long", MySqlFieldTypeEnum.BIGINT);
        TYPE_MAP.put("java.lang.Integer", MySqlFieldTypeEnum.INT);
        TYPE_MAP.put("java.lang.Boolean", MySqlFieldTypeEnum.BIT);
        TYPE_MAP.put("java.lang.Byte", MySqlFieldTypeEnum.BIT);
        TYPE_MAP.put("java.math.BigInteger", MySqlFieldTypeEnum.BIGINT);
        TYPE_MAP.put("java.lang.Float", MySqlFieldTypeEnum.FLOAT);
        TYPE_MAP.put("java.lang.Double", MySqlFieldTypeEnum.DOUBLE);
        TYPE_MAP.put("java.lang.Short", MySqlFieldTypeEnum.SMALLINT);
        TYPE_MAP.put("java.math.BigDecimal", MySqlFieldTypeEnum.DECIMAL);
        TYPE_MAP.put("java.sql.Date", MySqlFieldTypeEnum.DATE);
        TYPE_MAP.put("java.util.Date", MySqlFieldTypeEnum.DATE);
        TYPE_MAP.put("java.sql.Timestamp", MySqlFieldTypeEnum.DATETIME);
        TYPE_MAP.put("java.sql.Time", MySqlFieldTypeEnum.TIME);
        TYPE_MAP.put("java.time.LocalDateTime", MySqlFieldTypeEnum.DATETIME);
        TYPE_MAP.put("java.time.LocalDate", MySqlFieldTypeEnum.DATE);
        TYPE_MAP.put("java.time.LocalTime", MySqlFieldTypeEnum.TIME);
        TYPE_MAP.put("byte[]", MySqlFieldTypeEnum.BLOB);
        TYPE_MAP.put("long", MySqlFieldTypeEnum.BIGINT);
        TYPE_MAP.put("int", MySqlFieldTypeEnum.INT);
        TYPE_MAP.put("boolean", MySqlFieldTypeEnum.BIT);
        TYPE_MAP.put("byte", MySqlFieldTypeEnum.BIT);
        TYPE_MAP.put("float", MySqlFieldTypeEnum.FLOAT);
        TYPE_MAP.put("double", MySqlFieldTypeEnum.DOUBLE);
        TYPE_MAP.put("short", MySqlFieldTypeEnum.SMALLINT);
        TYPE_MAP.put("char", MySqlFieldTypeEnum.VARCHAR);
    }

    /**
     * 获取类型
     * @param className 类型名称
     * @return MySQL类型信息
     */
    public static MySqlTypeAndLength getTypeAndLength(String className){
        MySqlFieldTypeEnum typeEnum = TYPE_MAP.get(className);
        if (Objects.isNull(typeEnum)){
            return null;
        }
        return new MySqlTypeAndLength(
                typeEnum.toString().toLowerCase(),
                typeEnum.getParamCount(),
                typeEnum.getLengthDefault(),
                typeEnum.getPrecisionDefault()
                );
    }

}
