package cn.bootx.table.modify.mysql.util;

import cn.bootx.table.modify.mysql.entity.MySqlCreateParam;
import cn.bootx.table.modify.mysql.entity.MySqlModifyParam;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.experimental.UtilityClass;

/**
 * MySql表修改工具类
 * @author xxm
 * @since 2023/9/14
 */
@UtilityClass
public class MySqlTableModifyUtil {


    /**
     * 表名
     */
    public String tableName(MySqlCreateParam param){
        return StrUtil.format("CREATE TABLE `{}`",param.getName());
    }

    /**
     * 列
     */
    public String column(MySqlCreateParam param){
        return String.join(",", param.getColumns())+",";
    }

    /**
     * 主键索引
     */
    public String primaryKey(MySqlCreateParam param){
        if (StrUtil.isNotBlank(param.getKeys())){
            return StrUtil.format(" PRIMARY KEY {} USING BTREE,",param.getKeys());
        }
        return "";
    }

    /**
     * 索引
     */
    public String index(MySqlCreateParam param){
        if (CollUtil.isNotEmpty(param.getIndexes())) {
            String string = String.join(",",param.getIndexes());
            return string + ",";
        }
        return "";
    }

    /**
     * 表信息 引擎
     */
    public String engine(MySqlCreateParam param){
        if (StrUtil.isNotBlank(param.getEngine())){
            return StrUtil.format(" ENGINE = {} ",param.getEngine());
        }
        return "";
    }

    /**
     * 表信息 字符集
     */
    public String charset(MySqlCreateParam param){
        if (StrUtil.isNotBlank(param.getCharset())){
            return StrUtil.format(" CHARSET = {} ",param.getCharset());
        }
        return "";
    }

    /**
     * 表信息 备注
     */
    public String comment(MySqlCreateParam param){
        if (StrUtil.isNotBlank(param.getComment())){
            return StrUtil.format(" COMMENT = '{}' ",param.getComment());
        }
        return "";
    }


    /**
     * 表名
     */
    public String tableName(MySqlModifyParam param){
        return StrUtil.format("ALTER TABLE `{}`",param.getName());
    }

    /**
     * 删除列
     */
    public String dropColumn(MySqlModifyParam param){
        StringBuilder sb = new StringBuilder();
        for (String dropColumn : param.getDropColumns()) {
            sb.append(StrUtil.format(" DROP COLUMN {},",dropColumn));
        }
        return sb.toString();
    }

    /**
     * 更新列
     */
    public String updateColumn(MySqlModifyParam param){
        StringBuilder sb = new StringBuilder();
        for (String column : param.getModifyColumns()) {
            sb.append(StrUtil.format(" MODIFY COLUMN {},",column));
        }
        return sb.toString();
    }

    /**
     * 添加列
     */
    public String addColumn(MySqlModifyParam param){
        StringBuilder sb = new StringBuilder();
        for (String column : param.getAddColumns()) {
            sb.append(StrUtil.format(" ADD COLUMN {},",column));
        }
        return sb.toString();
    }

    /**
     * 主键索引
     */
    public String primaryKey(MySqlModifyParam param){
        if (StrUtil.isNotBlank(param.getKeys())){
            return StrUtil.format(" DROP PRIMARY KEY," +
                    " PRIMARY KEY {} USING BTREE,",param.getKeys());
        }
        return "";
    }

    /**
     * 删除索引
     */
    public String dropIndex(MySqlModifyParam param){
        StringBuilder sb = new StringBuilder();
        for (String dropIndex : param.getDropIndexes()) {
            sb.append(StrUtil.format(" DROP INDEX `{}`,",dropIndex));
        }
        return sb.toString();
    }

    /**
     * 新增索引
     */
    public String addIndex(MySqlModifyParam param){
        StringBuilder sb = new StringBuilder();
        for (String addIndex : param.getAddIndexes()) {
            sb.append(StrUtil.format(" ADD {},",addIndex));
        }
        return sb.toString();
    }

    /**
     * 表信息 引擎
     */
    public String engine(MySqlModifyParam param){
        if (StrUtil.isNotBlank(param.getEngine())){
            return StrUtil.format(" ENGINE = {},",param.getEngine());
        }
        return "";
    }

    /**
     * 表信息 字符集
     */
    public String charset(MySqlModifyParam param){
        if (StrUtil.isNotBlank(param.getCharset())){
            return StrUtil.format(" CHARSET = {},",param.getCharset());
        }
        return "";
    }

    /**
     * 表信息 备注
     */
    public String comment(MySqlModifyParam param){
        if (StrUtil.isNotBlank(param.getComment())){
            return StrUtil.format(" COMMENT = '{}',",param.getComment());
        }
        return "";
    }

}
