package cn.bootx.table.modify.mysql.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Mysql 类型和长度
 *
 * @author sunchenbin
 * @since 2020/5/27
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MySqlTypeAndLength{

    /** 字段类型 */
    private String type;

    /** 字段参数长度 */
    private int paramCount;

    /** 长度 */
    private Integer length;

    /** 小数长度 */
    private Integer precision;
}
