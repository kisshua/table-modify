package cn.bootx.table.modify.mysql.annotation;

import cn.bootx.table.modify.mysql.constants.MySqlEngineEnum;

import java.lang.annotation.*;

/**
 * MYSQL引擎类型
 * @author xxm
 * @since 2023/4/7
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DbMySqlEngine {

    MySqlEngineEnum value() default MySqlEngineEnum.DEFAULT;

}
