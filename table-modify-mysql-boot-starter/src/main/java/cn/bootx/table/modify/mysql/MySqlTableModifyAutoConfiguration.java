package cn.bootx.table.modify.mysql;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * MySQL自动配置扫描类
 *
 * @author xxm
 * @since 2023/4/6
 */
@ComponentScan
@ConditionalOnProperty(prefix = "table-modify", name = "database-type", havingValue = "mysql")
@ConfigurationPropertiesScan
public class MySqlTableModifyAutoConfiguration {

}
