package cn.bootx.table.modify.mysql.configuration;

import cn.bootx.table.modify.mysql.handler.MySqlStartUpHandler;
import cn.bootx.table.modify.mysql.handler.MySqlTableHandlerService;
import cn.bootx.table.modify.properties.TableModifyProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * 保证startUpHandler最优先执行
 * @author xxm
 * @since 2023/4/21
 */
@Order
@Configuration
@ConditionalOnProperty(prefix = "table-modify", name = "database-type", havingValue = "mysql")
public class MySqlTableModifyConfig {

    /**
     * 注入bean
     */
    @Bean
    @ConfigurationProperties(prefix = "table-modify")
    public TableModifyProperties tableModifyProperties(){
        return new TableModifyProperties();
    }

    /**
     * 创建Bean并执行
     */
    @Bean
    public MySqlStartUpHandler startUpHandler(
            MySqlTableHandlerService mySqlTableHandlerService,
            TableModifyProperties tableModifyProperties
    ) {
        MySqlStartUpHandler mysqlStartUpHandler = new MySqlStartUpHandler(mySqlTableHandlerService, tableModifyProperties);
        mysqlStartUpHandler.startHandler();
        return mysqlStartUpHandler;
    }
}
