package cn.bootx.table.modify.postgresql.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 建表参数
 * @author xxm
 * @since 2023/9/13
 */
@Getter
@Setter
@Accessors(chain = true)
public class PgSqlCreateParam {

    /** 表名称 */
    private String name;

    /** 表注释 */
    private String comment;

    /** 行列表 */
    private List<PgSqlEntityColumn> columns = new ArrayList<>();

    /** 主键列表 */
    private String keys;

    /** 索引列表 */
    private List<PgSqlEntityIndex> indexes = new ArrayList<>();
}
