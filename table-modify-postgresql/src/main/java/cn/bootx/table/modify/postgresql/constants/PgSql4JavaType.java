package cn.bootx.table.modify.postgresql.constants;

import cn.bootx.table.modify.postgresql.entity.PgSqlTypeAndLength;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * PgSql和Java类型映射关系
 * @author xxm
 * @since 2023/8/2
 */
public class PgSql4JavaType {
    private static final Map<String, PgSqlFieldTypeEnum> TYPE_MAP = new HashMap<>();

    static {
        TYPE_MAP.put("java.lang.String", PgSqlFieldTypeEnum.VARCHAR);
        TYPE_MAP.put("java.lang.Long", PgSqlFieldTypeEnum.INT8);
        TYPE_MAP.put("java.lang.Integer", PgSqlFieldTypeEnum.INT4);
        TYPE_MAP.put("java.lang.Boolean", PgSqlFieldTypeEnum.BIT);
        TYPE_MAP.put("java.lang.Byte", PgSqlFieldTypeEnum.BIT);
        TYPE_MAP.put("java.math.BigInteger", PgSqlFieldTypeEnum.INT8);
        TYPE_MAP.put("java.lang.Float", PgSqlFieldTypeEnum.FLOAT4);
        TYPE_MAP.put("java.lang.Double", PgSqlFieldTypeEnum.FLOAT8);
        TYPE_MAP.put("java.lang.Short", PgSqlFieldTypeEnum.INT2);
        TYPE_MAP.put("java.math.BigDecimal", PgSqlFieldTypeEnum.NUMERIC);
        TYPE_MAP.put("java.sql.Date", PgSqlFieldTypeEnum.TIMESTAMP);
        TYPE_MAP.put("java.util.Date", PgSqlFieldTypeEnum.TIMESTAMP);
        TYPE_MAP.put("java.sql.Timestamp", PgSqlFieldTypeEnum.TIMESTAMP);
        TYPE_MAP.put("java.sql.Time", PgSqlFieldTypeEnum.TIME);
        TYPE_MAP.put("java.time.LocalDateTime", PgSqlFieldTypeEnum.TIMESTAMP);
        TYPE_MAP.put("java.time.LocalDate", PgSqlFieldTypeEnum.DATE);
        TYPE_MAP.put("java.time.LocalTime", PgSqlFieldTypeEnum.TIME);
        TYPE_MAP.put("byte[]", PgSqlFieldTypeEnum.VARBIT);
        TYPE_MAP.put("long", PgSqlFieldTypeEnum.INT8);
        TYPE_MAP.put("int", PgSqlFieldTypeEnum.INT4);
        TYPE_MAP.put("boolean", PgSqlFieldTypeEnum.BIT);
        TYPE_MAP.put("byte", PgSqlFieldTypeEnum.BIT);
        TYPE_MAP.put("float", PgSqlFieldTypeEnum.FLOAT4);
        TYPE_MAP.put("double", PgSqlFieldTypeEnum.FLOAT8);
        TYPE_MAP.put("short", PgSqlFieldTypeEnum.INT2);
        TYPE_MAP.put("char", PgSqlFieldTypeEnum.VARCHAR);
    }

    /**
     * 获取类型
     * @param className 类型名称
     * @return PgSQL类型信息
     */
    public static PgSqlTypeAndLength getTypeAndLength(String className){
        PgSqlFieldTypeEnum typeEnum = TYPE_MAP.get(className);
        if (Objects.isNull(typeEnum)){
            return null;
        }
        return new PgSqlTypeAndLength(
                typeEnum.toString().toLowerCase(),
                typeEnum.getParamCount(),
                typeEnum.getLengthDefault(),
                typeEnum.getPrecisionDefault()
        );
    }
}
