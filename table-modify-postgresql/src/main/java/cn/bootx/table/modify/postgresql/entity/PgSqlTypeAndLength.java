package cn.bootx.table.modify.postgresql.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 字段类型
 * @author xxm
 * @since 2023/9/13
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PgSqlTypeAndLength {

    /** 字段类型 */
    private String type;

    /** 字段参数长度 */
    private int paramCount;

    /** 长度 */
    private Integer length;

    /** 小数长度 */
    private Integer precision;
}
