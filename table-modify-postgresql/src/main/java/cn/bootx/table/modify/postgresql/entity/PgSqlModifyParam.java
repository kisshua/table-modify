package cn.bootx.table.modify.postgresql.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class PgSqlModifyParam {
}
