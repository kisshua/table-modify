package cn.bootx.table.modify.postgresql.entity;

import cn.bootx.table.modify.constants.TableCharset;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 数据表信息
 * @author xxm
 * @since 2023/4/14
 */
@Getter
@Setter
@Accessors(chain = true)
public class PgSqlEntityTable {

    /** 表名称 */
    private String name;

    /** 表注释 */
    private String comment;

    /** 字符集 */
    @Deprecated
    private TableCharset charset;

    /** 主键列表 */
    private List<String> keys;

}
