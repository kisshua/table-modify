package cn.bootx.table.modify.postgresql.constants;

import cn.bootx.table.modify.postgresql.entity.PgSqlTypeAndLength;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用于配置pgsql数据库中类型，并且该类型需要设置几个长度 这里配置多少个类型决定了，创建表能使用多少类型 例如：varchar(1) decimal(5,2)
 * @author xxm
 * @since 2023/8/2
 */
@Getter
@AllArgsConstructor
public enum PgSqlFieldTypeEnum {
    /** 有符号2字节整数 */
    INT2(0,null,null),
    /** 有符号4字节整数 */
    INT4(0,null,null),
    /** 有符号的8字节整数 */
    INT8(0,null,null),
    /** 可选择精度的精确数字 推荐使用 NUMERIC */
    DECIMAL(2,8,2),
    /** 可选择精度的精确数字 */
    NUMERIC(2,8,2),
    /** 单精度浮点数（4字节） */
    FLOAT4(0,null,null),
    /** 双精度浮点数（8字节） */
    FLOAT8(0,null,null),
    /** 自动增长的2字节整数 */
    SERIAL2(0,null,null),
    /** 自动增长的4字节整数 */
    SERIAL4(0,null,null),
    /** 自动增长的8字节整数 */
    SERIAL8(0,null,null),
    /** 货币数量 */
    MONEY(0,null,null),
    /** 变长字符串 */
    VARCHAR(1,255,null),
    /** 定长字符串 */
    CHAR(1,255,null),
    /** 二进制数据（“字节数组”） */
    BYTEA(1,255,null),
    /** 变长字符串 */
    TEXT(0,null,null),
    /** 日历日期（年、月、日） */
    DATE(0,null,null),
    /** 日期和时间（无时区） */
    TIMESTAMP(1,6,null),
    /** 日期和时间，包括时区 */
    TIMESTAMPTZ(1,6,null),
    /** 一天中的时间（无时区） */
    TIME(1,6,null),
    /** 一天中的时间，包括时区 */
    TIMETZ(1,6,null),
    /** 时间段 */
    INTERVAL(1,6,null),
    /** 逻辑布尔值（真/假） 不可为空 */
    BOOL(0,null,null),
    /** 平面上的几何点 */
    POINT(0,null,null),
    /** 平面上的无限长的线 */
    LINE(0,null,null),
    /** 平面上的线段 */
    LSEG(0,null,null),
    /** 平面上的普通方框 */
    BOX(0,null,null),
    /** 平面上的几何路径 */
    PATH(0,null,null),
    /** 平面上的封闭几何路径 */
    POLYGON(0,null,null),
    /** 平面上的圆 */
    CIRCLE(0,null,null),
    /** IPv4或IPv6网络地址 */
    CIDR(0,null,null),
    /** IPv4或IPv6主机地址 */
    INET(0,null,null),
    /** MAC（Media Access Control）地址 */
    MACADDR(0,null,null),
    /** 定长位串 */
    BIT(1,1,null),
    /** 变长位串 */
    VARBIT(1,1,null),
    /** 文本搜索文档 */
    TSVECTOR(0,null,null),
    /** 文本搜索查询 */
    TSQUERY(0,null,null),
    /** 通用唯一标识码 */
    UUID(0,null,null),
    /** XML数据 */
    XML(0,null,null),
    /** 文本 JSON 数据 */
    JSON(0,null,null);

    /**
     * 字段类型参数个数:
     * 0. 不需要长度参数, 比如XML,JSON等类型
     * 1. 需要一个长度参数, 比如 TIME等类型
     * 2. 需要小数位数的, 比如 NUMERIC等类型
     */
    private final int paramCount;

    /** 默认长度 */
    private final Integer lengthDefault;

    /** 默认小数长度 */
    private final Integer precisionDefault;

    /**
     * 获取类型
     * @return MySQL类型信息
     */
    public PgSqlTypeAndLength getTypeAndLength(){
        return new PgSqlTypeAndLength(
                this.toString().toLowerCase(),
                this.getParamCount(),
                this.getLengthDefault(),
                this.getPrecisionDefault()
        );
    }
}
