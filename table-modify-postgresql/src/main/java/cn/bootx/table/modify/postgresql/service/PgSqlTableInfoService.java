package cn.bootx.table.modify.postgresql.service;

import cn.bootx.table.modify.postgresql.entity.PgSqlEntityColumn;
import cn.bootx.table.modify.postgresql.entity.PgSqlEntityTable;
import cn.bootx.table.modify.postgresql.entity.PgSqlModifyMap;
import cn.bootx.table.modify.postgresql.util.PgSqlInfoUtil;
import cn.bootx.table.modify.utils.ColumnUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 表信息变动
 * @author xxm
 * @since 2023/9/14
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PgSqlTableInfoService {


    /**
     * 获取创建表的信息
     */
    public void getCreateTable(Class<?> clas, PgSqlModifyMap baseTableMap){
        String tableName = ColumnUtils.getTableName(clas);

        // 获取表注释
        String comment = ColumnUtils.getTableComment(clas);

        // 获取主键
        List<String> keys = getEntityKeys(clas);

        PgSqlEntityTable entityTable = new PgSqlEntityTable()
                .setName(tableName)
                .setComment(comment)
                .setKeys(keys);
        baseTableMap.getCreateTables().add(entityTable);
    }

    /**
     * 获取实体类上配置的主键
     */
    private List<String> getEntityKeys(Class<?> clas){
        List<PgSqlEntityColumn> entityColumns = PgSqlInfoUtil.getEntityColumns(clas);
        return entityColumns.stream()
                .filter(PgSqlEntityColumn::isKey)
                .map(PgSqlEntityColumn::getName)
                .collect(Collectors.toList());
    }

    /**
     * 更新表信息
     */
    public void getModifyTable(Class<?> clas,PgSqlModifyMap baseTableMap){

    }
}
