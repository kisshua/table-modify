package cn.bootx.table.modify.postgresql.service;

import cn.bootx.table.modify.postgresql.entity.PgSqlModifyMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 更新表处理
 * @author xxm
 * @since 2023/9/14
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PgSqlModifyTableService {

    /**
     * 根据传入的map创建或修改表结构
     * @param baseTableMap 操作sql的数据结构
     */
    public void modifyTableConstruct(PgSqlModifyMap baseTableMap) {

    }
}
