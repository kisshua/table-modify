package cn.bootx.table.modify.postgresql.annotation;

import cn.bootx.table.modify.postgresql.constants.PgSqlIndexType;

import java.lang.annotation.*;

/**
 * 索引注解
 * @author xxm
 * @since 2023/8/2
 */
@Target({ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(value = DbPgSqlIndexes.class)
@Documented
public @interface DbPgSqlIndex {

    /**
     * 索引的类型
     */
    PgSqlIndexType type() default PgSqlIndexType.BTREE;

    /**
     * 唯一索引
     */
    boolean unique() default false;

    /**
     * 并发
     */
    boolean concurrently() default false;
    /**
     * 索引的名字, 不设置默认为索引类型+用_分隔的字段名计划
     */
    String name() default "";

    /**
     * 要建立索引的数据库字段名，可设置多个建立联合索引{"login_mobile","login_name"}, 只可以在类上使用, 字段上使用不生效
     */
    String[] columns() default {};

    /**
     * 要建立索引的实体类字段名，会自动转换为数据库字段名称，
     * 可设置多个建立联合索引{"login_mobile","login_name"}, 只可以在类上使用, 字段上使用不生效
     * 如果 {columns} 同时配置的话，本配置不生效
     */
    String[] fields() default {};

    /**
     * 索引注释
     */
    String comment() default "";
}
