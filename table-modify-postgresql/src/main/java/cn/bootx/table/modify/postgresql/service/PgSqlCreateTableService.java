package cn.bootx.table.modify.postgresql.service;

import cn.bootx.table.modify.postgresql.entity.*;
import cn.bootx.table.modify.postgresql.handler.PgSqlTableModifyDao;
import cn.bootx.table.modify.postgresql.util.PgSqlInfoUtil;
import cn.bootx.table.modify.postgresql.util.PgSqlTableModifyUtil;
import cn.bootx.table.modify.properties.TableModifyProperties;
import cn.hutool.core.collection.CollUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 建表处理
 * @author xxm
 * @since 2023/9/14
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PgSqlCreateTableService {
    private final TableModifyProperties tableModifyProperties;

    private final PgSqlTableModifyDao pgSqlTableModifyDao;


    /**
     * 根据map结构创建表
     * @param modifyMap 用于存需要创建表的数据
     */
    public void createTable(PgSqlModifyMap modifyMap) {
        // 做创建表操作
        for (PgSqlEntityTable table : modifyMap.getCreateTables()) {
            log.info("开始创建表：" + table.getName());
            try {
                PgSqlCreateParam createParam = this.buildCreateParam(table, modifyMap);
                // 表字段为空不进行创建
                if (CollUtil.isEmpty(createParam.getColumns())){
                    log.warn("扫描发现 {} 没有建表字段，请检查！",createParam.getName());
                }
                String createSql = this.buildCreateSql(createParam);
                pgSqlTableModifyDao.createTable(createSql);
                log.info("完成创建表：" + table.getName());
            } catch (Exception e){
                log.error("创建表失败：" + table.getName(),e);
                // 快速失败
                if (tableModifyProperties.isFailFast()){
                    throw e;
                }
            }
        }
    }


    /**
     * 构建建表条件
     */
    private PgSqlCreateParam buildCreateParam(PgSqlEntityTable table, PgSqlModifyMap modifyMap){
        PgSqlCreateParam pgSqlCreateParam = new PgSqlCreateParam();
        // 表基础信息
        pgSqlCreateParam.setName(table.getName())
                .setComment(table.getComment());

        // 字段
        List<PgSqlEntityColumn> columns = modifyMap.getAddColumns()
                .get(table.getName());
        if (CollUtil.isNotEmpty(columns)) {
            pgSqlCreateParam.setColumns(columns);
        }
        // 主键
        List<String> keys = table.getKeys();
        if (CollUtil.isNotEmpty(keys)){
            pgSqlCreateParam.setKeys(PgSqlInfoUtil.buildBracketParams(keys));
        }
        // 索引
        List<PgSqlEntityIndex> indexes = modifyMap.getAddIndexes().get(table.getName());
        if (CollUtil.isNotEmpty(indexes)){
            pgSqlCreateParam.setIndexes(indexes);
        }

        return pgSqlCreateParam;
    }


    /**
     * 构建建表语句
     */
    private String buildCreateSql(PgSqlCreateParam param) {
        StringBuilder sql = new StringBuilder();
        // 表名
        sql.append(PgSqlTableModifyUtil.tableName(param));
        sql.append("(");
        //  列创建语句
        sql.append(PgSqlTableModifyUtil.columnCreate(param));
        //  主键
        sql.append(PgSqlTableModifyUtil.primaryKey(param));
        // 去除最后的逗号
        sql.deleteCharAt(sql.length()-1);
        sql.append(");");
        //  索引
        sql.append(PgSqlTableModifyUtil.indexCreate(param));
        // 字段备注
        sql.append(PgSqlTableModifyUtil.columnComment(param));
        // 索引备注
        sql.append(PgSqlTableModifyUtil.indexComment(param));
        // 表备注
        sql.append(PgSqlTableModifyUtil.comment(param));
        return sql.toString();
    }
}
