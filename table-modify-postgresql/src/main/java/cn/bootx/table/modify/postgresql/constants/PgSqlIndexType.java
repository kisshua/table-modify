package cn.bootx.table.modify.postgresql.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 索引类型
 * @author xxm
 * @since 2023/9/13
 */
@Getter
@AllArgsConstructor
public enum PgSqlIndexType {
    /**
     * 普通（B-树）索引
     */
    BTREE("btree","index_",null),
    /**
     * Hash（哈希）索引
     */
    HASH("hash","",""),

    /**
     * GiST（广义搜索树）索引
     */
    GIST("gist","",""),

    /**
     *  SP-GiST（空间路径广义搜索树）索引
     */
    SPGiST("spgist","",""),

    /**
     * GIN（倒排索引）索引
     */
    GIN("gin","",""),

    /**
     * BRIN（块范围索引）索引
     */
    BRIN("brin","","");

    /** 索引名称 */
    private final String name;
    /** 索引名称前缀 */
    private final String prefix;
    /** 所使用的索引方式 */
    private final String using;
}
