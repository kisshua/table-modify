package cn.bootx.table.modify.postgresql.entity;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 用于存放创建表的字段信息
 * @author xxm
 * @since 2023/9/13
 */
@Getter
@Setter
@Accessors(chain = true)
public class PgSqlEntityColumn {


    /** 字段名 */
    private String name;

    /** 排序 */
    private int order;

    /** 字段类型 */
    private String fieldType;

    /**
     * 字段类型参数个数:
     * 0. 不需要长度参数, 比如date类型
     * 1. 需要一个长度参数, 比如 int/datetime/varchar等
     * 2. 需要小数位数的, 比如 decimal/float等
     */
    private int paramCount;

    /** 长度 */
    private Integer length;

    /** 小数类型的浮点长度 */
    private Integer precision;

    /** 字段是否非空 */
    private boolean fieldIsNull;

    /** 字段是否是主键 */
    private boolean key;

    /** 主键是否自增 */
    @Deprecated
    private boolean autoIncrement;

    /** 字段默认值 */
    private String defaultValue;

    /** 是否需要删除 */
    private boolean delete;

    /** 字段的备注 */
    private String comment;

    /**
     * 建表的字段语句
     *   "aa" int2 NOT NULL,
     *   "a1" time(6) NOT NULL,
     *   "2" timetz(6),
     *   "a5" xml,
     *   "123" json,
     */
    public String toColumn(){
        // 字段名
        StringBuilder sb = new StringBuilder("\"").append(getName()).append("\"");
        // 数据类型
        sb.append(" ").append(getFieldType());
        // 字段长度
        if (getParamCount() == 1){
            sb.append("(").append(getLength()).append(")");
        }
        // 小数位数
        if (getParamCount() == 2){
            sb.append("(")
                    .append(getLength())
                    .append(",")
                    .append(getPrecision())
                    .append(")");
        }
        // 是否可以为空
        if (isFieldIsNull()){
            sb.append(" NULL");
        } else {
            sb.append(" NOT NULL");
        }
        // 默认值
        if (StrUtil.isNotBlank(getDefaultValue())){
            sb.append(" DEFAULT ").append(getDefaultValue());
        }
        sb.append(",");
        return sb.toString();
    }

    /**
     * 字段备注
     * COMMENT ON COLUMN "table"."column" IS '备注';
     */
    public String toColumnComment(String tableName){
        return StrUtil.format("COMMENT ON COLUMN \"{}\".\"{}\" IS '{}';",tableName,getName(),getComment());
    }

}
