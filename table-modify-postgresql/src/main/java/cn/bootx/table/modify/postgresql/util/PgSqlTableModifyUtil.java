package cn.bootx.table.modify.postgresql.util;

import cn.bootx.table.modify.postgresql.entity.PgSqlCreateParam;
import cn.bootx.table.modify.postgresql.entity.PgSqlEntityColumn;
import cn.bootx.table.modify.postgresql.entity.PgSqlEntityIndex;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.experimental.UtilityClass;

import java.util.stream.Collectors;

/**
 * PgSql 表修改工具类
 * @author xxm
 * @since 2023/9/14
 */
@UtilityClass
public class PgSqlTableModifyUtil {

    /**
     * 表名
     */
    public String tableName(PgSqlCreateParam param){
        return StrUtil.format("CREATE TABLE \"{}\"",param.getName());
    }

    /**
     * 列创建
     */
    public String columnCreate(PgSqlCreateParam param){
        return param.getColumns().stream()
                .map(PgSqlEntityColumn::toColumn)
                .collect(Collectors.joining());
    }
    /**
     * 列创建
     */
    public String columnComment(PgSqlCreateParam param){
        return param.getColumns().stream()
                .map(col->col.toColumnComment(param.getName()))
                .collect(Collectors.joining());
    }

    /**
     * 主键索引
     */
    public String primaryKey(PgSqlCreateParam param){
        if (StrUtil.isNotBlank(param.getKeys())){
            return StrUtil.format("PRIMARY KEY {},",param.getKeys());
        }
        return "";
    }

    /**
     * 索引创建语句
     */
    public String indexCreate(PgSqlCreateParam param){
        if (CollUtil.isNotEmpty(param.getIndexes())) {
            // 索引列表
            return param.getIndexes().stream()
                    .map(index -> index.toIndexCreate(param.getName()))
                    .collect(Collectors.joining());
        }
        return "";
    }
    /**
     * 索引备注语句
     */
    public String indexComment(PgSqlCreateParam param){
        if (CollUtil.isNotEmpty(param.getIndexes())) {
            // 索引列表
            return param.getIndexes().stream()
                    .map(PgSqlEntityIndex::toIndexComment)
                    .collect(Collectors.joining());
        }
        return "";
    }


    /**
     * 表信息 备注
     */
    public String comment(PgSqlCreateParam param){
        if (StrUtil.isNotBlank(param.getComment())){
            return StrUtil.format("COMMENT  ON TABLE \"{}\" IS '{}';",param.getName(),param.getComment());
        }
        return "";
    }


}
