package cn.bootx.table.modify.postgresql.handler;

import cn.bootx.table.modify.annotation.DbTable;
import cn.bootx.table.modify.constants.UpdateType;
import cn.bootx.table.modify.postgresql.entity.PgSqlModifyMap;
import cn.bootx.table.modify.postgresql.service.*;
import cn.bootx.table.modify.properties.TableModifyProperties;
import cn.bootx.table.modify.utils.ClassScanner;
import cn.bootx.table.modify.utils.ColumnUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 建表处理器服务类
 * @author xxm
 * @since 2023/9/13
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PgSqlTableHandlerService {

    private final TableModifyProperties tableModifyProperties;

    private final PgSqlTableModifyDao pgSqlTableModifyDao;

    private final PgSqlIndexInfoService pgSqlIndexInfoServiceService;

    private final PgSqlColumnInfoService pgSqlColumnInfoService;

    private final PgSqlCreateTableService pgSqlCreateTableService;

    private final PgSqlModifyTableService pgSqlModifyTableService;

    private final PgSqlTableInfoService pgSqlTableInfoService;

    /**
     * 读取配置文件的三种状态（创建表、更新表、不做任何事情）
     */
    public void startModifyTable() {
        log.debug("开始执行PgSql的表处理方法");

        // 自动创建模式：update表示更新，create表示删除原表重新创建
        UpdateType updateType = tableModifyProperties.getUpdateType();

        // 要扫描的entity所在的pack
        String pack = tableModifyProperties.getScanPackage();

        // 拆成多个pack，支持多个
        String[] packs = pack.split("[,;]");

        // 从包package中获取所有的Class
        Set<Class<?>> classes = ClassScanner.scan(packs, DbTable.class);

        // 初始化用于存储各种操作表结构的容器
        PgSqlModifyMap baseTableMap = new PgSqlModifyMap();

        // 表名集合
        List<String> tableNames = new ArrayList<>();

        // 循环全部的entity
        for (Class<?> clas : classes) {
            // 没有注解不需要创建表 或者配置了忽略建表的注解
            if (!ColumnUtils.hasHandler(clas)) {
                continue;
            }
            // 添加要处理的表, 同时检查是表是否合法
            this.addAndCheckTable(tableNames, clas);
            // 构建出全部表的增删改的map
            this.buildTableMapConstruct(clas, baseTableMap, updateType);
        }

        // 根据传入的信息，分别去创建或修改表结构
        this.createOrModifyTableConstruct(baseTableMap);
    }

    /**
     * 构建出全部表的增删改的map
     * @param clas package中的entity的Class
     * @param baseTableMap 用于存储各种操作表结构的容器
     */
    private void buildTableMapConstruct(Class<?> clas, PgSqlModifyMap baseTableMap,
                                        UpdateType updateType) {
        // 获取entity的tableName
        String tableName = ColumnUtils.getTableName(clas);

        // 如果配置文件配置的是DROP_CREATE，表示将所有的表删掉重新创建
        if (updateType == UpdateType.DROP_CREATE) {
            log.debug("由于配置的模式是DROP_CREATE，因此先删除表后续根据结构重建，表名：{}", tableName);
            baseTableMap.getDropTables().add(tableName);
        }

        // 先查该表是否以存在
        if (pgSqlTableModifyDao.existsByTableName(tableName)){
            // 获取表更新信息
            pgSqlTableInfoService.getModifyTable(clas,baseTableMap);
            // 获取表更新字段
            pgSqlColumnInfoService.getModifyColumn(clas,baseTableMap);
            // 获取更新索引
            pgSqlIndexInfoServiceService.getModifyIndex(clas,baseTableMap);
        } else {
            // 获取建表信息
            pgSqlTableInfoService.getCreateTable(clas,baseTableMap);
            // 获取建表字段
            pgSqlColumnInfoService.getCreateColumn(clas,baseTableMap);
            // 获取建表索引
            pgSqlIndexInfoServiceService.getCreateIndex(clas,baseTableMap);
        }
    }

    /**
     * 根据传入的信息，分别去创建或修改表结构
     */
    private void createOrModifyTableConstruct(PgSqlModifyMap baseTableMap){
        // 1. 删除表
        baseTableMap.getDropTables().forEach(pgSqlTableModifyDao::dropTableByName);
        // 2. 创建表
        pgSqlCreateTableService.createTable(baseTableMap);
        // 3. 修改表结构
        pgSqlModifyTableService.modifyTableConstruct(baseTableMap);
    }

    /**
     * 检查名称
     */
    private void addAndCheckTable(List<String> tableNames, Class<?> clas) {
        String tableName = ColumnUtils.getTableName(clas);
        if (tableNames.contains(tableName)) {
            throw new RuntimeException(tableName + "表名出现重复，禁止创建！");
        }
        tableNames.add(tableName);
    }
}
