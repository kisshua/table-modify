package cn.bootx.table.modify.postgresql.service;

import cn.bootx.table.modify.postgresql.entity.PgSqlEntityColumn;
import cn.bootx.table.modify.postgresql.entity.PgSqlModifyMap;
import cn.bootx.table.modify.postgresql.util.PgSqlInfoUtil;
import cn.bootx.table.modify.utils.ColumnUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 字段数据处理类
 * @author xxm
 * @since 2023/8/2
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PgSqlColumnInfoService {

    /**
     * 获取建表的字段
     */
    public void getCreateColumn(Class<?> clas,  PgSqlModifyMap baseTableMap){
        // 获取entity的tableName
        String tableName = ColumnUtils.getTableName(clas);

        // 用于实体类配置的全部字段
        List<PgSqlEntityColumn> entityColumns;
        try {
            entityColumns = PgSqlInfoUtil.getEntityColumns(clas).stream()
                    .filter(o->!o.isDelete())
                    .collect(Collectors.toList());
            baseTableMap.getAddColumns().put(tableName,entityColumns);
        } catch (Exception e) {
            log.error("表：{}，初始化字段结构失败！", tableName);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 获取要更新的字段
     */
    public void getModifyColumn(Class<?> clas,  PgSqlModifyMap baseTableMap) {

    }
}
