package cn.bootx.table.modify.postgresql.handler;

import cn.bootx.table.modify.postgresql.entity.*;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 数据库操作类
 * @author xxm
 * @since 2023/9/13
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class PgSqlTableModifyDao {
    private final JdbcTemplate jdbcTemplate;

    /**
     * 新建表
     */
    @SuppressWarnings("SqlSourceToSinkFlow")
    @Transactional(rollbackFor = Exception.class)
    public void createTable(String createTableSql){
        jdbcTemplate.execute(createTableSql);
    }

    /**
     * 更新表
     */
    @SuppressWarnings("SqlSourceToSinkFlow")
    @Transactional(rollbackFor = Exception.class)
    public void modifyTable(String modifyTableSql){
        jdbcTemplate.execute(modifyTableSql);

    }


    /**
     * 查据表名询表信息
     * @param tableName 表结构的map
     * @return MySqlTableInfo
     */
    public boolean existsByTableName(String tableName){
        String sql = "SELECT EXISTS (SELECT FROM pg_tables WHERE tablename = ?);";
        return Boolean.TRUE.equals(jdbcTemplate.queryForObject(sql, new SingleColumnRowMapper<>(Boolean.class), tableName));
    }

    /**
     * 查据表名询表信息
     * @param tableName 表结构的map
     * @return MySqlTableInfo
     */
    public PgSqlTableInfo findTableByTableName(String tableName){
        return  null;
    }

    /**
     * 根据表名查询库中该表的字段结构等信息
     * @param tableName 表结构的map
     * @return 表的字段结构等信息
     */
    public List<PgSqlTableColumn> findColumnByTableName(String tableName){
        return null;
    }

    /**
     * 查询当前表存在的索引(除了主键索引primary)
     * @param tableName 表名
     * @return 索引名列表
     */
    public List<PgSqlTableIndex> findIndexByTableName(String tableName){
        return null;
    }

    /**
     * 查询当前表存在的主键索引
     * @param tableName 表名
     * @return 索引名列表
     */
    public List<PgSqlTableIndex> findPrimaryIndexByTableName(String tableName){
        return null;
    }

    /**
     * 根据表名删除表
     * @param tableName 表名
     */
    @SuppressWarnings("SqlSourceToSinkFlow")
    public void dropTableByName(String tableName){
        String sql = StrUtil.format("DROP TABLE IF EXISTS \"{}\";",tableName);
        jdbcTemplate.execute(sql);
    }
}
