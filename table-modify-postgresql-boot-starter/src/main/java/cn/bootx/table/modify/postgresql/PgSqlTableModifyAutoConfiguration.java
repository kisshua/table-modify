package cn.bootx.table.modify.postgresql;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * Postgresql自动配置扫描类
 * @author xxm
 * @since 2023/8/2
 */
@ComponentScan
@ConfigurationPropertiesScan
@ConditionalOnProperty(prefix = "table-modify", name = "database-type", havingValue = "postgresql")
public class PgSqlTableModifyAutoConfiguration {
}
