package cn.bootx.table.modify.postgresql.configuration;

import cn.bootx.table.modify.postgresql.handler.PgSqlStartUpHandler;
import cn.bootx.table.modify.postgresql.handler.PgSqlTableHandlerService;
import cn.bootx.table.modify.properties.TableModifyProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * 保证startUpHandler最优先执行
 * @author xxm
 * @since 2023/4/21
 */
@Order
@Configuration
@ConditionalOnProperty(prefix = "table-modify", name = "database-type", havingValue = "postgresql")
public class PgSqlTableModifyConfig {

    /**
     * 注入bean
     */
    @Bean
    @ConfigurationProperties(prefix = "table-modify")
    public TableModifyProperties tableModifyProperties(){
        return new TableModifyProperties();
    }

    /**
     * 创建Bean并执行
     */
    @Bean
    public PgSqlStartUpHandler startUpHandler(
            PgSqlTableHandlerService pgSqlTableHandlerService,
            TableModifyProperties tableModifyProperties
    ) {
        PgSqlStartUpHandler pgSqlStartUpHandler = new PgSqlStartUpHandler(pgSqlTableHandlerService, tableModifyProperties);
        pgSqlStartUpHandler.startHandler();
        return pgSqlStartUpHandler;
    }
}
